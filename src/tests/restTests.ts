import { ITestSuite } from '../interfaces';
import { Category, EndpointReturn, Order, Product, TestResults, User, UserPatchRequest } from '../types';
import { avgRuntime } from '../testing';
import { v4 } from "uuid";
import constants from '../constants';

export default class RestTestSuite implements ITestSuite {
    private endpoint: string;

    constructor(ip: string) {
        this.endpoint = `http://${ip}:3000`;
    }

    public async testRandomProduct(): Promise<EndpointReturn> {
        try {
            const resp = await fetch(this.endpoint + "/randomproduct");
            if (!resp.ok) throw new Error("Failed to fetch random product");
            const json = await resp.json();
            return {
                payload: json,
                ok: true
            };
        } catch (e) {
            console.error(e);
            return {
                payload: null,
                ok: false
            };
        }
    }

    public async testUserById(id: string): Promise<EndpointReturn> {
        try {
            const resp = await fetch(this.endpoint + "/user/" + id);
            if (!resp.ok) throw new Error("Failed to fetch user by id");
            const json = await resp.json();
            return { payload: json, ok: true };
        } catch (e) {
            console.error("error");
            return { payload: null, ok: false };
        }
    }

    public async testAllProducts(categoryId?: string): Promise<EndpointReturn> {
        try {
            const resp = await fetch(this.endpoint + "/products" + (categoryId ? "?categoryId=" + categoryId : ""));
            if (!resp.ok) throw new Error("Failed to fetch all products");
            const json = await resp.json();
            return { payload: json, ok: true };
        } catch (e) {
            console.error(e);
            return { payload: null, ok: false };
        }
    }

    public async testProductById(productId: string): Promise<EndpointReturn> {
        try {
            const resp = await fetch(this.endpoint + "/product/" + productId);
            if (!resp.ok) throw new Error("Failed to fetch product by id");
            const json = await resp.json();
            return { payload: json, ok: true };
        } catch (e) {
            console.error(e);
            return { payload: null, ok: false };
        }
    }

    public async testAllCategories(): Promise<EndpointReturn> {
        try {
            const resp = await fetch(this.endpoint + "/categories");
            if (!resp.ok) throw new Error("Failed to fetch all categories");
            const json = await resp.json();
            return { payload: json, ok: true };
        } catch (e) {
            console.error(e);
            return { payload: null, ok: false };
        }
    }

    public async testAllOrders(): Promise<EndpointReturn> {
        try {
            const resp = await fetch(this.endpoint + "/allorders");
            if (!resp.ok) throw new Error("Failed to fetch all orders");
            const json = await resp.json();
            return { payload: json, ok: true };
        } catch (e) {
            console.error(e);
            return { payload: null, ok: false };
        }
    }

    public async testOrdersByUser(id: string): Promise<EndpointReturn> {
        try {
            const resp = await fetch(this.endpoint + "/orders?id=" + id);
            if (!resp.ok) throw new Error("Failed to fetch orders by user");
            const json = await resp.json();
            return { payload: json, ok: true };
        } catch (e) {
            console.error(e);
            return { payload: null, ok: false };
        }
    }

    public async testOrderById(id: string): Promise<EndpointReturn> {
        try {
            const resp = await fetch(this.endpoint + "/order/" + id);
            if (!resp.ok) throw new Error("Failed to fetch order by id");
            const json = await resp.json();
            return { payload: json, ok: true };
        } catch (e) {
            console.error(e);
            return { payload: null, ok: false };
        }
    }

    public async testAllUsers(): Promise<EndpointReturn> {
        try {
            const resp = await fetch(this.endpoint + "/users");
            if (!resp.ok) throw new Error("Failed to fetch all users");
            const json = await resp.json();
            return { payload: json, ok: true };
        } catch (e) {
            console.error(e);
            return { payload: null, ok: false };
        }
    }

    public async testInsertOrder(order: Order): Promise<EndpointReturn> {
        try {
            const resp = await fetch(this.endpoint + "/orders", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(order)
            });
            if (!resp.ok) throw new Error("Failed to insert order");
            return { payload: null, ok: true }
        } catch (e) {
            console.error(e);
            return { payload: null, ok: false }
        }
    }

    public async testUpdateUser(patch: UserPatchRequest): Promise<EndpointReturn> {
        try {
            const resp = await fetch(this.endpoint + `/user/${patch.id}`, {
                method: "PATCH",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(patch)
            });
            if (!resp.ok) throw new Error("Failed to update user");
            return { payload: null, ok: true }
        } catch (e) {
            console.error(e);
            return { payload: null, ok: false }
        }
    }

    public async runSuite(iterations: number): Promise<TestResults> {
        const results: TestResults = {
            randomProduct: {
                ok: false,
                time: 0
            },
            userById: {
                ok: false,
                time: 0
            },
            allProducts: {
                ok: false,
                time: 0
            },
            productById: {
                ok: false,
                time: 0
            },
            allCategories: {
                ok: false,
                time: 0
            },
            allOrders: {
                ok: false,
                time: 0
            },
            ordersByUser: {
                ok: false,
                time: 0
            },
            orderById: {
                ok: false,
                time: 0
            },
            allUsers: {
                ok: false,
                time: 0
            },
            insertOrder: {
                ok: false,
                time: 0
            },
            updateUser: {
                ok: false,
                time: 0
            }
        };

        results.randomProduct = await avgRuntime(async () => await this.testRandomProduct(), iterations);
        results.userById = await avgRuntime(async () => await this.testUserById(constants.TEST_USER_ID), iterations, constants.EXPECTED_USER);
        results.allProducts = await avgRuntime(async () => await this.testAllProducts(), iterations, constants.EXPECTED_PRODUCTS);
        results.productById = await avgRuntime(async () => await this.testProductById(constants.TEST_PRODUCT_ID), iterations, constants.EXPECTED_PRODUCT);
        results.allCategories = await avgRuntime(async () => await this.testAllCategories(), iterations, constants.EXPECTED_CATEGORIES);
        results.allOrders = await avgRuntime(async () => await this.testAllOrders(), iterations, constants.EXPECTED_ORDERS);
        results.ordersByUser = await avgRuntime(async () => await this.testOrdersByUser(constants.TEST_USER_ID), iterations, constants.EXPECTED_ORDERSBYUSER);
        results.orderById = await avgRuntime(async () => await this.testOrderById(constants.TEST_ORDER_ID), iterations, constants.EXPECTED_ORDER);
        results.allUsers = await avgRuntime(async () => await this.testAllUsers(), iterations, constants.EXPECTED_USERS);
        results.insertOrder = await avgRuntime(async () => await this.testInsertOrder({ id: v4(), ...constants.TEST_ORDER }), iterations);
        results.updateUser = await avgRuntime(async () => await this.testUpdateUser(constants.TEST_UPDATE), iterations);

        return results;
    }
}