import categoriesOracle from './oracles/categories.json';
import ordersOracle from './oracles/orders.json';
import productsOracle from './oracles/products.json';
import usersOracle from './oracles/users.json';

const constants = {
    TEST_USER_ID: usersOracle[0].id,
    TEST_PRODUCT_ID: productsOracle[0].id,
    TEST_ORDER_ID: ordersOracle[0].id,
    TEST_ORDER: {
        userId: usersOracle[1].id,
        products: [{ productId: productsOracle[0].id, quantity: 1 }],
        totalAmount: 8
    },
    TEST_UPDATE: {
        id: usersOracle[1].id,
        password: "test123"
    },

    EXPECTED_CATEGORIES: categoriesOracle,
    EXPECTED_USER: usersOracle[0],
    EXPECTED_USERS: usersOracle,
    EXPECTED_PRODUCT: productsOracle[0],
    EXPECTED_PRODUCTS: productsOracle,
    EXPECTED_ORDER: ordersOracle[0],
    EXPECTED_ORDERS: ordersOracle,
    EXPECTED_ORDERSBYUSER: ordersOracle.filter((order) => order.userId === usersOracle[0].id)
};

export default constants;